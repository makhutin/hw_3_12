//
//  GiftsServiceTest.swift
//  Homework3Tests
//
//  Created by Алексей Махутин on 08.07.2020.
//  Copyright © 2020 Алексей Махутин. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import HW_3_12

internal final class GiftsServiceTest: XCTestCase {

    var service = GiftsService()

    override func setUp() {
        super.setUp()

        self.service = GiftsService()
    }

    func testLoadMusikArtist() {
        let expectation = self.expectation(description: "FooBarBaz")

        stub(condition: { _ -> Bool in
            return true
        }) { _ -> HTTPStubsResponse in
            guard let path = OHPathForFileInBundle("Gifts.json", Bundle(for: Self.self)) else {
                preconditionFailure("Could not find file")
            }

            return HTTPStubsResponse(fileAtPath: path, statusCode: 200, headers: [
                "Content-Type":"application/json"
            ])
        }
        self.service.loadData { data in
            XCTAssertNotNil(data)
            XCTAssertTrue(data?.providers.first?.id == 1)
            XCTAssertTrue(data?.providers.first?.title == "Amazon.com")
            expectation.fulfill()
        }

        self.waitForExpectations(timeout: 5, handler: nil)
    }
}
