//
//  GiftsService.swift
//  HW_3_12
//
//  Created by Алексей Махутин on 
//  Copyright © 2020 Алексей Махутин. All rights reserved.
//

import Foundation
import Alamofire

struct GiftDescription: Codable {
    let id: Int
    let title: String
}

struct Gifts: Codable {
    let id: Int
    let title: String
    let image_url: String
    let gift_cards: [GiftDescription]
}

struct GiftCardsData: Codable {
    let providers: [Gifts]
}

internal final class GiftsService {

    func loadData(completion: @escaping (GiftCardsData?) -> Void) {
        guard let url = URL(string: "http://mobile-hr.de02.agima.ru/exam/providers.json") else { return }

        AF.request(url).responseData { answer in
            if let data = answer.data {
                let jsonDecoder = JSONDecoder()
                let result = try? jsonDecoder.decode(GiftCardsData.self, from: data)
                completion(result)
            } else {
                completion(nil)
            }
        }
    }
}
