//
//  SecondViewController.swift
//  HW_3_12
//
//  Created by Алексей Махутин on 06.08.2020.
//  Copyright © 2020 Алексей Махутин. All rights reserved.
//

import UIKit
import FirebaseAnalytics

internal final class SecondViewController: UIViewController {

    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var stackView: UIStackView!

    var data: Gifts?

    override func viewDidLoad() {
        super.viewDidLoad()

        Analytics.logEvent("segue", parameters: [
            AnalyticsParameterContentType: "complete"
        ])

        self.idLabel.text = self.data?.id.description
        self.titleLabel.text = self.data?.title

        DispatchQueue.global().async {
            guard let url = URL(string: self.data?.image_url ?? ""),
                let data = try? Data(contentsOf: url) else { return }

            DispatchQueue.main.async {
                self.imageView.image = UIImage(data: data)
            }
        }

        self.data?.gift_cards.forEach({ giftData in
            let label = self.createLabelWithText(text: giftData.title)
            self.stackView.addArrangedSubview(label)
        })
    }

    private func createLabelWithText(text: String) -> UILabel {
        let label = UILabel()
        label.text = text
        label.textAlignment = .center
        return label
    }
}
