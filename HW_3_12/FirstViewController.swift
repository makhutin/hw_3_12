//
//  ViewController.swift
//  HW_3_12
//
//  Created by Алексей Махутин on 06.08.2020.
//  Copyright © 2020 Алексей Махутин. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class FirstViewController: UIViewController {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!

    private var data: GiftCardsData?

    private lazy var timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { _ in
        let dataformater = DateFormatter()
        dataformater.dateFormat = "HH:mm:ss"
        let stringTime = dataformater.string(from: Date())
        self.timeLabel.text = stringTime
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.timer.fire()
        let service = GiftsService()
        service.loadData { data in
            self.data = data
            self.tableView.reloadData()
            Analytics.logEvent("download", parameters: [
                AnalyticsParameterContentType: "download == \(data == nil)"
            ])
        }

        self.tableView.dataSource = self
        self.tableView.delegate = self
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        guard let cell = sender as? UITableViewCell,
            let indexPath = self.tableView.indexPath(for: cell),
            let vc = segue.destination as? SecondViewController else { return }

        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterContentType: "\(indexPath.row)"
        ])
        vc.data = self.data?.providers[indexPath.row]
    }
}

extension FirstViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data?.providers.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "gifts", for: indexPath)
        let dataForCell = self.data?.providers[indexPath.row]
        cell.textLabel?.text = dataForCell?.title
        return cell
    }
}
  
